from PyQt5.QtCore import Qt, QUrl
from PyQt5.QtWidgets import QApplication, QWidget, QTextBrowser, QVBoxLayout, QPushButton
from PyQt5.QtGui import QDesktopServices


def refreshInfo(textBrowser):
    import requests
    import datetime

    textBrowser.clear()

    request_json = requests.get("https://www.reddit.com/user/McJty/submitted/top.json?sort=new&limit=100",
                                headers={'User-agent': 'Chrome'}).json()

    for index in reversed(range(100)):
        response = request_json["data"]["children"][index]["data"]
        textBrowser.append(datetime.datetime.fromtimestamp(int(response["created"])).strftime('%c'))
        textBrowser.append(response["title"])
        textBrowser.append("<a href=\"https://www.reddit.com" + response["permalink"] + "\">" + response["permalink"] + "</a>")
        textBrowser.append("--------------------")

if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)

    window = QWidget()
    window.resize(800, 600)
    window.move(300, 300)
    window.setWindowTitle("McJty RSS Feed Reader")

    layout = QVBoxLayout(window)

    # Thread browser
    textBrowser = QTextBrowser(window)
    textBrowser.setTextInteractionFlags(Qt.TextBrowserInteraction)
    textBrowser.setOpenExternalLinks(True)
    layout.addWidget(textBrowser)

    # Refresh button
    refreshButton = QPushButton("Refresh", window)
    refreshButton.clicked.connect(lambda: refreshInfo(textBrowser))
    layout.addWidget(refreshButton)

    # Get McJty's Mods button
    mcjtyButton = QPushButton("Get McJty's Mods", window)
    mcjtyButton.clicked.connect(lambda: QDesktopServices.openUrl(QUrl("http://minecraft.curseforge.com/members/McJty/projects")))
    layout.addWidget(mcjtyButton)

    refreshInfo(textBrowser)

    window.show()

    sys.exit(app.exec_())
