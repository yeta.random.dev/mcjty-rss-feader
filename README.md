Since I don't have a MAC OS install people using MAC OS will need to either
run the python script or make the package themselves.
<br />
<br />
![Alt text](http://i.imgur.com/FPi8EnW.png "Preview")
<br />
<br />
# License - LGPLv3
# Stuff used
```
Python 3.5
PyQt5.6
requests 2.5.1 (it seems any version after this does not work with pyinstaller)
pyinstaller
```